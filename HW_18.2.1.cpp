﻿#include <iostream>
#include <stack>

using namespace std;

template <typename T>
void SomeStack (T* masel, int x)
{
    cout << "Все элементы структуры данных стек: ";

    stack <int> fstack;
    for (int i = 0; i < x; ++i)
    {
        fstack.push(*(masel + i));
        cout << *(masel + i) << " ";
    }

    cout << "\n" << "Верхний элемент: " << fstack.top();
    cout << "\n" << "Удаление верхнего элемента.";
    fstack.pop();
    cout << "\n" << "Новый верхний элемент: " << fstack.top();
}

int main()
{
    setlocale(LC_CTYPE, "rus");
    cout << "Введите размер структуры данных стек: ";

    int x;
    cin >> x;

    int* arr = new int [x]; 

    for (int i = 0; i < x; i++)
    {
        arr[i] = i;
    }

    SomeStack(arr ,x);

    delete arr;
    arr = nullptr;
}